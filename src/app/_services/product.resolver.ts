import { Resolve } from "@angular/router";
import { Product } from "../_models/product.model";
import { Injectable } from "@angular/core";
import { ProductService } from "./product.service";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

@Injectable()
export class ProductResolver implements Resolve<Product> {
    constructor(private productService: ProductService,
                private router: Router) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> {
        let id = +route.params['id'];

        if (isNaN(id)){
            console.log('Invalid product id: ${id}');
            this.router.navigate(['/products']);
            return Observable.of(null);
        }

        return this.productService.getProduct(id)
            .map(product => { 
                if (product) {
                    return product;
                }
                this.router.navigate(['/products']);
                return null;                
            })
            .catch(error => {
                console.log(`Error occurred retrieving product: ${error}`);
                this.router.navigate(['/products']);
                return Observable.of(null);
            });
    }
}