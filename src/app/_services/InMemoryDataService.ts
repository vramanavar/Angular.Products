import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users = [
      { id: 1, firstName: 'Nikau', lastName: 'Hohepa', email: 'nhohepa@gmail.com', password: '', 
                addressType: 'home', street1: '#123 Willcott Street', street2: '', city: 'Auckland', state: '', zip: '1025' },

      { id: 2, firstName: 'Anahera', lastName: 'Kahurangi', email: 'akahurangi@yahoo.com', password: '',  
                addressType: 'home', street1: '@423 Mairoa Street', street2: '', city: 'Hamilton', state: '', zip: '2709' },

      { id: 3, firstName: 'Moana', lastName: 'Tane', email: 'mtane@hotmail.com', password: '',   
                addressType: 'home', street1: '#20 Wairara Tce', street2: '', city: 'Wellington', state: '', zip: '8790' },

      { id: 4, firstName: 'Patariki', lastName: 'Huhana', email: 'phuhana@gmail.com', password: '',   
                addressType: 'home', street1: '#71 Woodward Road', street2: '', city: 'Kerikeri', state: '', zip: '6060' },

      { id: 5, firstName: 'Ataahua', lastName: 'Manaia', email: 'amanaia@gmail.com', password: '',  
                addressType: 'home', street1: '#34 Sea View Pl', street2: '', city: 'Rotorua', state: '', zip: '2610' },

      { id: 6, firstName: 'Tama', lastName: 'Tamati', email: 'ttamati@pwc.com', password: '',   
                addressType: 'home', street1: '#431 Clyve Ave', street2: '', city: 'Whangerei', state: '', zip: '4071' },

      { id: 7, firstName: 'Manaia', lastName: 'Petera', email: 'mpetera@css.com', password: '',   
                addressType: 'home', street1: '#113 Old Town Road', street2: '', city: 'Kerosene Creek', state: '', zip: '7799' },

      { id: 8, firstName: 'Dr IQ', lastName: 'Tama', email: 'itama@hotmail.com', password: '',   
                addressType: 'home', street1: '#166 Twin Brids View', street2: '', city: 'Piha', state: '', zip: '3104' },

      { id: 9, firstName: 'Ngaire', lastName: 'Anahera', email: 'nanahera@crossz.com', password: '',   
                addressType: 'v', street1: '#120 Wards Tce', street2: '', city: 'Cape Reinga', state: '', zip: '5544' },

      { id: 10, firstName: 'Patariki', lastName: 'Kahurangi', email: 'pkahurangi@tnz.com', password: '',   
                addressType: 'home', street1: '#861 Tauranga Road', street2: '', city: 'Tauranga', state: '', zip: '3140' }
    ];
    return {users};
  }
}