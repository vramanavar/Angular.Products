import { Product } from '../_models/product.model';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

export class ProductService {
    lastIdGenerated: number = 25;
    dataChange: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);
    OnProductChanged = new Subject<Product[]>();
    
    products: Product[] = [
        new Product(1, 'DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(2, 'Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(3, 'Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(4, '4-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(5, '5-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(6, '6-Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(7, '7-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(8, '8-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(9, '9-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(10, '10-Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(11, '11-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(12, '12-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(13, '13-Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(14, '14-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(15, '15-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(16, '16-Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(17, '17-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(18, '18-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(19, '19-Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(20, '20-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(21, '21-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(22, '22-Docker', 'Docker', '../../assets/docker.png', 3),
        new Product(23, '23-DotNet', 'MS DotNet', '../../assets/dotnet.jpg', 1),
        new Product(24, '24-Angular.5+', 'Angular.5+', '../../assets/angular.jpg', 2),
        new Product(25, '25-Docker', 'Docker', '../../assets/docker.png', 3)   
    ];

    public getProducts(): Observable<Product[]> {
        this.dataChange.next(this.products.slice());
        return this.dataChange;
    }

    public getProduct(id: number): Observable<Product>{
        return  Observable.of(this.products.find(x => x.id == id));
    }

    public addProduct(product: Product){
        product.id = ++this.lastIdGenerated;
        this.products.push(product);
        this.OnProductChanged.next(this.products.slice());
        this.dataChange.next(this.products.slice());
    }

    public updateProduct(id: number, product: Product){
        var index = this.products.indexOf(this.products.find(x => x.id == id));
        this.products[index] = product;
        this.products[index].id = id;
        this.OnProductChanged.next(this.products.slice());
        this.dataChange.next(this.products.slice());
    }

    public deleteProduct(id: number){
        var index = this.products.indexOf(this.products.find(x => x.id == id));
        this.products.splice(index , 1);
        this.OnProductChanged.next(this.products.slice());
        this.dataChange.next(this.products.slice());
    }


}