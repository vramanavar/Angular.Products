import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../_models/user.model';

@Injectable()
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) { }

  get isLoggedIn(){
    return this.loggedIn.asObservable();
  }

  login(user: User){
    if (user.email != '' && user.password != ''){
      this.loggedIn.next(true);
      this.router.navigate(['/']);
    }
  }

  logout(){
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }
  
}
