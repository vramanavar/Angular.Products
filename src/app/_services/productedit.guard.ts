import { CanDeactivate } from "@angular/router";
import { ProductEditComponent } from "../products/productedit/productedit.component";
import { Injectable } from "@angular/core";

@Injectable()
export class ProductEditGuard implements CanDeactivate<ProductEditComponent> {

    canDeactivate(component: ProductEditComponent): boolean {

        if (component.isDirty){
            let productName = component.Product.name || 'New Product';
            return confirm(`Navigate away and lost all changes to ${productName}`);
        }

        return true;
    }
}