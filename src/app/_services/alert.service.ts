import { Injectable } from '@angular/core';

@Injectable()
export class AlertService {
    public messages: string[] = ['Sample Default Message'];
    isDisplayed = false;
    
    addMessage(message: string): void {
        let currentDate = new Date();
        this.messages.unshift(message + ' at ' + currentDate.toLocaleString());
    }
}
