import { Category } from '../_models/category.model';

export class GenericService {

    categories: Category[] = [
        new Category(1, "Cross Platform Technology"),
        new Category(2, "SPA Framework"),
        new Category(3, "Container Technology")
    ];

    public getCategories(){
        return this.categories.slice();
    }
}