import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../_models/user.model';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { InMemoryDataService } from './InMemoryDataService';
import { Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService  {
    private usersUrl = '/api/users';
    private customUrl = `${environment.baseUrl}/Users/GetUsers`;

    constructor(private http: HttpClient){}

    getUsers (): Observable<User[]> {
        return this.http.get<User[]>(this.usersUrl)
        .pipe(
            tap(users => this.log(`fetched users`)),
            catchError(this.handleError('getHeroes', []))
        );
    }

    findUsers(id: number, filter = '', sortOrder = 'asc', pageNumber = 0, pageSize = 3): Observable<User[]> {
        let httpParams = new HttpParams().set('filter', filter)
        .set('sortDirection', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString());

        return this.http.get(this.customUrl, {params: httpParams})
        .pipe(
            tap(users => this.log(`findUsers: fetched users`)),
            catchError(this.handleError('getHeroes', null)) 
        ); 
    }
    
    getUser(id: number): Observable<User> {
        const url = `${this.usersUrl}/${id}`;
        return this.http.get<User>(url)
            .pipe(
                tap(_ => this.log(`fetched user id ${id}`)),
                catchError(this.handleError<User>(`getUser=${id}`))
            );
    }

    updateUser(user: User): Observable<any> {
        return this.http.put(this.usersUrl, user)
            .pipe(
                tap(_ => this.log(`Updated User with id: ${user.id}`)),
                catchError(this.handleError<User>(`udate user with id: ${user.id}`))
            );
    }

    addUser(user: User): Observable<User> {
        return this.http.post<User>(this.usersUrl, user)
            .pipe(
                tap(_ => this.log(`added new user`)),
                catchError(this.handleError<User>('add new user'))
            );
    }

    deleteUser(user: User | number): Observable<User> {
        const id = typeof user === 'number'? user : user.id;
        const url = `${this.usersUrl}/${id}`;

        return this.http.delete<User>(url)
            .pipe(
                tap(_ => this.log(`deleted user ${id}`)),
                catchError(this.handleError<User>('deleted user'))
            );
    }

    private handleError<T> (operation = 'operation', result?: T){
        return (error: any): Observable<T> => {
            console.error(error);
            this.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        };
    }

    private log(err: any): any {
        console.log(err);
    }
}