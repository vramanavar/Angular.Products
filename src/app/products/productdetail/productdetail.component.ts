import { Component, OnInit, Inject } from '@angular/core';
import { ProductService } from '../../_services/product.service';
import { Product } from '../../_models/product.model';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { DOCUMENT } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { ProductEditComponent } from '../productedit/productedit.component';

@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.less']
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  selectedProduct: Product;
  id: number;
  onProductSelectionChanged: Subscription;

  constructor(private dialogRef: MatDialogRef<ProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,
    private dialog: MatDialog,
    private productService: ProductService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.selectedProduct = data['product'];
      this.data.id = this.selectedProduct.id;
    }); 
  }

  onClose(){
    this.dialogRef.close();
  }

  ngOnDestroy(){
  }

  onEditProduct(){
    const dialogRef = this.dialog.open(ProductEditComponent, {
      data: {id: this.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        console.log('product.id', this.id);
      }
    });    
  }

  onDeleteProduct(){
    this.productService.deleteProduct(this.id);
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
