import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ProductListComponent } from './productlist/productlist.component';
import { ProductsHomeComponent } from './productshome/productshome.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';
import { ProductEditComponent } from './productedit/productedit.component';
import { ProductResolver } from '../_services/product.resolver';
import { ProductEditGuard } from '../_services/productedit.guard';
import { AuthGuard } from '../_guards/auth.guard';

const productRoutes: Routes = [
    { path: '', component: ProductsHomeComponent, 
        children: [
            {path: 'products', component: ProductListComponent},
            {path: 'new', component: ProductEditComponent},
            {path: ':id', component: ProductDetailComponent, 
                    resolve: { product: ProductResolver },
                     },
            {path: ':id/edit', component: ProductEditComponent,
                    resolve: { product: ProductResolver },
                    canDeactivate: [ ProductEditGuard ] }
        ]
    }  
];

@NgModule({
    imports: [
        RouterModule.forChild(productRoutes)
    ],
    exports: [ RouterModule ]
})
export class ProductsRoutingModule {}
