import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Product } from '../../_models/product.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ProductService } from '../../_services/product.service';
import { GenericService } from '../../_services/generic.service';
import { Category } from '../../_models/category.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-productedit',
  templateUrl: './productedit.component.html',
  styleUrls: ['./productedit.component.less']
})
export class ProductEditComponent implements OnInit {
  id: number;
  editMode: boolean;
  productForm: FormGroup;
  categories: Category[];
  private currentProduct: Product;
  private originalProduct: Product;

  get Product(): Product {
    return this.currentProduct;
  }

  set Product(value: Product){
    this.currentProduct = value;
    this.originalProduct = Object.assign({}, value);
  }

  get isDirty(): boolean {
    return JSON.stringify(this.originalProduct) != JSON.stringify(this.currentProduct);
  }

  constructor(
    private dialogRef: MatDialogRef<ProductEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,    
    private productService: ProductService,
    private genericService: GenericService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.id = this.data.id;
    this.editMode = this.data.id != null;
    this.initForm();
    this.categories = this.genericService.getCategories();
  }

  private initForm(){
    let productName = '';
    let description = '';
    let imagePath = '';
    let categoryId = 0;
    let flavourId = 1;

    if (this.editMode){
      //read from productservice
      this.productService.getProduct(this.id).subscribe(x => {
        this.Product = x;
        productName = x.name;
        description = x.description;
        imagePath = x.imagePath;
        categoryId = x.categoryId;
        flavourId = x.flavourId;        
      });
    }

    this.productForm = new FormGroup({
      'name': new FormControl(productName, Validators.required),
      'description': new FormControl(description, Validators.required),
      'imagePath': new FormControl(imagePath, Validators.required),
      'categoryId': new FormControl(categoryId, Validators.required),
      'flavourId': new FormControl(flavourId, Validators.required)
    })
  }

  onSubmit(){
    if (this.editMode){
      this.productService.updateProduct(this.id, this.productForm.value);
    }else{
      this.productService.addProduct(this.productForm.value);
    }
    this.onClose();
  }

  onClose(){
    this.dialogRef.close();
  }

  validateFormControls(){
    var catId = this.productForm.get('categoryId');
    var flaId = this.productForm.get('flavourId');

    if (catId != null && flaId != null){
      return true;
    }
    return false;
  }

}
