import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../_models/product.model';

@Component({
  selector: 'product-item',
  templateUrl: './productitem.component.html',
  styleUrls: ['./productitem.component.less']
})
export class ProductItemComponent implements OnInit {
  
  @Input() product: Product;

  constructor() { }

  ngOnInit() {
  }

} 
