import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ProductService } from '../../_services/product.service';
import { Subscription } from 'rxjs/Subscription';
import { Product } from '../../_models/product.model';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatDialog, MatSort } from '@angular/material';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import { ProductDetailComponent } from '../productdetail/productdetail.component';
import { ProductEditComponent } from '../productedit/productedit.component';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.less']
})
export class ProductListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  products: Product[] = [];
  displayedColumns = ['id', 'name', 'description', 'isActive'];
  dataSource: ProductDataSource | null;

  constructor(private productService: ProductService,
    private router: Router, private route: ActivatedRoute,
    private dialog: MatDialog) { 
  /*
    this.productChangedSubscription = this.productService.OnProductChanged.subscribe((productsList) => {
      this.products = productsList;
    }); */
  }

  openDialog(id: number): void {

    let dialogRef = this.dialog.open(ProductDetailComponent,{
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Dialog was closed!');
    });
  }

  openEditDialog(id: number): void {
    let dialogRef = this.dialog.open(ProductEditComponent,{
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Edit Dialog was closed!');
    });
  }  

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
  }
 
  ngOnInit() {
    //this.products = this.productService.getProducts();
    this.dataSource = new ProductDataSource(this.productService, this.paginator, this.sort);
    console.log(this.dataSource.data().length);
  }

  ngOnDestroy() {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
  }


  onNewProduct(){
    console.log('NewClicked!');
    this.router.navigate(['new'], { relativeTo: this.route});
  }
}

export class ProductDataSource extends DataSource<any> {

  constructor(private productService: ProductService, private paginator: MatPaginator,
              private sort: MatSort) {
    super();
  }

  connect(): Observable<Product[]> {
    const displayDataChanges = [
      this.productService.getProducts(),
      this.paginator.page,
      this.sort.sortChange
    ];
    
    return Observable.merge(...displayDataChanges).map(() => {
      var dbData = this.productService.products.slice();
      // Grab the page's slice of data.

      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;

      var data = dbData.splice(startIndex, this.paginator.pageSize);
      if (!this.sort.active || this.sort.direction == '') 
      { 
        return data; 
      }
      else
      {
        return data.sort((a, b) => {
          let propertyA: number|string = '';
          let propertyB: number|string = '';
     
          switch (this.sort.active) {
            case 'Id': [propertyA, propertyB] = [a.id, b.id]; break;            
            case 'Name': [propertyA, propertyB] = [a.name , b.name]; break;
          }
     
          let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
          let valueB = isNaN(+propertyB) ? propertyB : +propertyB;
     
          return (valueA < valueB ? -1 : 1) * (this.sort.direction == 'desc' ? 1 : -1);
        }); 
      }     
    });
  }

  data(){
    return this.productService.products.slice();
  }

  disconnect(): void {
  }
}