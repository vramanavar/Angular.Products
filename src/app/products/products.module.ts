import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from '../_shared/shared.module';
import { ProductListComponent } from './productlist/productlist.component';
import { ProductsRoutingModule } from './products.routing.module';
import { ProductsHomeComponent } from './productshome/productshome.component';
import { ProductItemComponent } from './productlist/productitem/productitem.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';
import { ProductEditComponent } from './productedit/productedit.component';
import { MaterialModule } from '../_shared/material.module';
import { ProductResolver } from '../_services/product.resolver';

@NgModule({
    declarations: [
        ProductListComponent,
        ProductsHomeComponent,
        ProductItemComponent,
        ProductDetailComponent,
        ProductEditComponent
    ],
    imports: [
        SharedModule,
        ReactiveFormsModule,
        MaterialModule,
        ProductsRoutingModule
    ],
    providers: [
        ProductResolver
    ]
})
export class ProductsModule {} 