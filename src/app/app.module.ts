import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CoreModule } from './_core/core.module';
import { AppRoutingModule } from './app.routing.module';
import { ProductService } from './_services/product.service';
import { SharedModule } from './_shared/shared.module';
import { GenericService } from './_services/generic.service';
import { MaterialModule } from './_shared/material.module';
import { AuthService } from './_services/auth.service';
import { AuthGuard } from './_guards/auth.guard';
import { FlexLayoutModule } from "@angular/flex-layout";
import { AlertService } from './_services/alert.service';
import { SelectiveStrategy } from './_guards/selective.strategy';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from '../app/_services/InMemoryDataService';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UserService } from './_services/user.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreInterceptor } from './_shared/core.interceptor';

@NgModule({ 
  declarations: [
    AppComponent
  ],
  imports: [ 
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MaterialModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CoreInterceptor,
      multi: true
    },
    ProductService, 
    GenericService, 
    AuthService, 
    AuthGuard, 
    AlertService, 
    SelectiveStrategy, 
    UserService 
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
