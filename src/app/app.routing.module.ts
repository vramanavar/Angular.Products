import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./_core/home/home.component";
import { LoginComponent } from "./_core/login/login.component";
import { AuthGuard } from "./_guards/auth.guard";
import { SignupComponent } from "./_core/signup/signup.component";
import { ProfileComponent } from "./_core/profile/profile.component";
import { PageNotFoundComponent } from "./_core/pagenotfound/pagenotfound.component";
import { AlertsComponent } from "./_core/alerts/alerts.component";
import { SelectiveStrategy } from "./_guards/selective.strategy";

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [ AuthGuard ] },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'profile', component: ProfileComponent },
    { 
        path: 'products', 
        canActivate: [ AuthGuard ], 
        data: { preload: true },
        loadChildren: './products/products.module#ProductsModule' },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { enableTracing: true }),
        RouterModule.forChild([{
            path: 'alerts', 
            component: AlertsComponent,
            outlet: 'popup'
            }])
        ],
    exports: [RouterModule]
})
export class AppRoutingModule {}