import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router,
    CanLoad
} from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import { Route } from '@angular/compiler/src/core';

@Injectable()
export class AuthGuard implements CanActivate
{
    constructor(
        private authService: AuthService,
        private router: Router
    ){}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.authService.isLoggedIn       // {1}
          .take(1)                               // {2} 
          .map((isLoggedIn: boolean) => {        // {3}
            if (!isLoggedIn){
              this.router.navigate(['/login']);  // {4}
              return false;
            }
            return true;
          });
      }

      canLoad(route: Route): Observable<boolean> {
        return this.authService.isLoggedIn.take(1).map( (isLoggedIn: boolean) => {
            if (!isLoggedIn){
                this.router.navigate(['/login']);  // {4}
                return false;
              }
              return true;
        });
      }
}