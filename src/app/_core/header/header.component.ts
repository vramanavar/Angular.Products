import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { Observable } from 'rxjs/Observable';
import { MatDialog, MatSidenav } from '@angular/material';
import { AboutComponent } from '../about/about.component';
import { Router } from '@angular/router';
import { AlertService } from '../../_services/alert.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>; 
  environmentName: string;

  @Input('SideNavBar') sidenav : MatSidenav;

  constructor(private authService: AuthService, 
              private alertService: AlertService,
                private dialog: MatDialog,
                private router: Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.environmentName = environment.envName;
  }

  onLogout(){
    this.authService.logout();
  }

  onAboutDialog(){
    this.dialog.open(AboutComponent, {
      height: '200px',
      width: '250px',
      data: {
        a: 'This is test sentence'
      }
    });
  }

  onSignUp(){
    this.router.navigate(['signup']);
  }

  onProfile(){
    //Redirecting to primary and secondary outlets at once
    this.router.navigate([{
      outlets: {
        primary: 'profile',
        popup: 'alerts'
      }
    }]);
  }

  onAlerts(){
    this.router.navigate([{outlets: { popup: 'alerts'}}]);
    this.alertService.isDisplayed = true;
  }

  onAlertsHide(){
    this.router.navigate([{outlets: { popup: null }}]);
    this.alertService.isDisplayed = false;    
  }

  onHomeClick(){
    this.router.navigate(['home']);
  }
}