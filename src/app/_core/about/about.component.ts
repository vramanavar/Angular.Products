import { MAT_DIALOG_DATA } from "@angular/material";
import { Inject, Component } from "@angular/core";

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html'
  })
  export class AboutComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
  }