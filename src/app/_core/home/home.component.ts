import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../_services/user.service';
import { User } from '../../_models/user.model';
import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import { BehaviorSubject, of, merge } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, finalize } from 'rxjs/operators';
import { ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { tap } from 'rxjs/internal/operators/tap';
import { AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs/internal/observable/fromEvent';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';
import { distinctUntilChanged } from 'rxjs/internal/operators/distinctUntilChanged';
import "rxjs/add/operator/takeWhile";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements AfterViewInit, OnInit, OnDestroy {

  user: User;
  dataSource: UsersDataSource;
  displayedColumns = ["id", "firstName", "lastName", "email", "addressType", "street1", "street2", "city", "state", "zip"];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;
  private isAlive: boolean = true;
  
  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.user =  this.route.snapshot.data["user"];
    this.dataSource = new UsersDataSource(this.userService);
    this.dataSource.loadUsers(1);
  }

  ngOnDestroy(){
    this.isAlive = false;
  }

  ngAfterViewInit() {

    //Search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadUsersPage();
        })
      )
      .subscribe();

    //Sort
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    //Merging of observables
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadUsersPage())
      )
      .takeWhile(() => this.isAlive)
      .subscribe();
  }
 
  loadUsersPage() { 
    this.dataSource.loadUsers(
      ((this.user) && (this.user.id)) ? this.user.id : 0,
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
  }


  onSelect(user: User): void {
    console.log(`Selected User: ${user.firstName}`);
  }

  onRowClicked(row) {
    console.log('Row clicked: ', row);
  }
}

export class UsersDataSource implements DataSource<User> {

  private usersSubject = new BehaviorSubject<User[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private isAlive: boolean = true;
  public loading$ = this.loadingSubject.asObservable();

  constructor(private userService: UserService) {}

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
      return this.usersSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      this.usersSubject.complete();
      this.loadingSubject.complete();
      this.isAlive = false;
  }

  loadUsers(userId: number, filter = '', sortDirection = 'asc', pageIndex = 0, pageSize = 3) {

      this.loadingSubject.next(true);

      this.userService.findUsers(userId, filter, sortDirection, pageIndex, pageSize)
        .pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
          )
        .takeWhile(() => this.isAlive)
        .subscribe((users: User[]) => this.usersSubject.next(users));
  }    
}
