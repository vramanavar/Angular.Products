import { Component, OnInit } from '@angular/core';
import { AboutComponent } from '../about/about.component';
import { MatDialog } from '@angular/material';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: 'products', title: 'Products',  icon:'content_paste', class: '' },
    { path: 'profile', title: 'User Profile',  icon:'person', class: '' },    
    { path: 'signup', title: 'Signup',  icon:'library_books', class: '' },
    { path: 'about', title: 'About',  icon:'library_books', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SideBarComponent implements OnInit {
  menuItems: any[];

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      return false;
  };

  onAboutDialog(){
    this.dialog.open(AboutComponent, {
      height: '200px',
      width: '250px',
      data: {
        a: 'This is test sentence'
      }
    });
  }  
}