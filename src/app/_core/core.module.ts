import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from '../app.routing.module';
import { SharedModule } from '../_shared/shared.module';
import { MaterialModule } from '../_shared/material.module';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AboutComponent } from './about/about.component';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { AlertsComponent } from './alerts/alerts.component';
import { SideBarComponent } from './sidebar/sidebar.component';

@NgModule({
    declarations: [
        HeaderComponent,
        HomeComponent,
        LoginComponent,
        AboutComponent,
        SignupComponent,
        ProfileComponent,
        PageNotFoundComponent,
        AlertsComponent,
        SideBarComponent
    ],
    entryComponents: [AboutComponent],
    imports: [
        SharedModule,
        MaterialModule,
        ReactiveFormsModule,
        AppRoutingModule        
    ],
    exports: [
        AppRoutingModule,
        SharedModule,
        HeaderComponent,
        SideBarComponent
    ]
})
export class CoreModule {}