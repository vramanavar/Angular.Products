import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder, ValidatorFn, AbstractControl, FormArray } from '@angular/forms';
import { User } from '../../_models/user.model';
import {  } from '@angular/forms/src/model';
import { NumberValidators } from '../../_shared/number.validators';
import { EmailValidators } from '../../_shared/email.validators';
import 'rxjs/add/operator/debounceTime';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  user: User = new User();
  emailMessage: string;

  private validationMessages = {
    required: 'Please enter your email address',
    pattern: 'Please enter valid email address'
  };

  constructor(private fb: FormBuilder,
  private router: Router) { }

  ngOnInit() {
    this.profileForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(10)]],
      emailGroup: this.fb.group({
        email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+')]],
        confirmEmail: ['', Validators.required]
      }, {validator: EmailValidators.emailMatcher}),
      phone: '',
      notification: 'email',
      rating: ['', NumberValidators.rangeCheck(1, 5)],
      addresses:  this.fb.array([this.buildAddress()])
    });

    this.profileForm.get('notification').valueChanges.subscribe(value => {
      this.setNotification(value);
    });

    const emailControl = this.profileForm.get('emailGroup.email');
    emailControl.valueChanges.debounceTime(1000).subscribe(value => {
      this.setMessage(emailControl);
    });    
  }

  get addresses(): FormArray {
    return <FormArray>this.profileForm.get('addresses');
  }

  addAddress(){
    this.addresses.push(this.buildAddress());
  }

  buildAddress() {
    return this.fb.group({
      addressType: 'home',
      street1: '',
      street2: '',
      city: '',
      state: '',
      zip: ''
    });
  }
  setMessage(c: AbstractControl): void {
    this.emailMessage = '';
    if ((c.touched || c.dirty) && c.errors){
      this.emailMessage = Object.keys(c.errors).map(key => 
        this.validationMessages[key]).join('');
    }
  }

  onSave(){
    console.log(this.profileForm);
    console.log('Saved: ' + JSON.stringify(this.profileForm.value));
    //Call Service to persist data to datastore
  }

  setNotification(notifyVia: string): void {
    const phoneControl = this.profileForm.get('phone');
    if (notifyVia === 'text'){
      phoneControl.setValidators(Validators.required);
    }else{
      phoneControl.clearValidators();
    }
    phoneControl.updateValueAndValidity();
  }

  onCancel(){
    this.router.navigate(['']);
  }
}
