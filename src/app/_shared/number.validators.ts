import { ValidatorFn, AbstractControl } from "@angular/forms";

export class NumberValidators {
    static rangeCheck(min: number, max: number): ValidatorFn {
        return(c: AbstractControl): {[key: string]: boolean} | null => {
            if (c.value !== undefined && (isNaN(c.value) || c.value < min || c.value > max)) {
              return { 'rangeCheck': true };
            };
            return null;
        };
      }
}