import { AbstractControl } from "@angular/forms";

export class EmailValidators {
    static emailMatcher(c: AbstractControl): {[key:string] : boolean} | null {
        let emailControl = c.get('email');
        let confirmEmailControl = c.get('confirmEmail');
        
        if (emailControl.pristine || confirmEmailControl.pristine){
          return null;
        }
      
        if (emailControl.value == confirmEmailControl.value){
          return null;
        }
      
        return { 'emailMatcher': true};
      }
}