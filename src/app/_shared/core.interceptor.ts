import { HttpInterceptor } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HttpRequest } from "@angular/common/http";
import { HttpHandler } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { HttpEvent } from "@angular/common/http";

@Injectable()
export class CoreInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const modified = req.clone(
            {
                setHeaders: {
                                'Content-Type': 'application/json',
                                'Test-Header': 'Test'
                            }
            });
        console.log('Core Interceptor called!');
        return next.handle(modified);
    }

}