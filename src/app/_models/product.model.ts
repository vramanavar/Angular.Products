export class Product{
    public id: number;
    public name: string;
    public description: string;
    public imagePath: string;
    public categoryId: number;
    public flavourId: number;
    public isActive: boolean;

    constructor(id: number, name: string, desc: string, imagePath: string, 
        categoryId: number = 0, flavourId: number = 1, isActive: boolean = true) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.imagePath = imagePath;
        this.categoryId = categoryId;
        this.flavourId = flavourId;
        this.isActive = isActive;
    }
}