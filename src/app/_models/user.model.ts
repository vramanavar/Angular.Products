  export class User {
    public id: number;
    public firstName?: string;
    public lastName?: string;
    public email?: string;
    public password?: string;
    public addressType = 'home';
    public street1?: string;
    public street2?: string;
    public city?: string;
    public state?: string;
    public zip?: string;

    constructor(
        id?: number,
        firstName?: string,
        lastName?: string,
        email?: string,
        password?: string,
        addressType = 'home',
        street1?: string,
        street2?: string,
        city?: string,
        state?: string,
        zip?: string) { 
          this.id = id;
          this.firstName = firstName;
          this.lastName = lastName;
          this.email = email;
          this.password = password;
          this.addressType = addressType;
          this.street1 = street1;
          this.street2 = street2;
          this.city = city;
          this.state = state;
          this.zip = zip;         
        }
    }   