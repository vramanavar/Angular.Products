# Angular.Products

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

12th Apr 2018 - Upgraded CLI to 1.7.4 and Code upgrade to 6.0.0-rc.1 (as latest RCs were breaking with Bazel compiler build failures due to conflict with rxjs/rxjs-compat versions)
Complete dependencies as below: 

    "@angular/animations": "6.0.0-rc.1",
    "@angular/cdk": "^5.2.4",
    "@angular/common": "6.0.0-rc.1",
    "@angular/compiler": "6.0.0-rc.1",
    "@angular/core": "6.0.0-rc.1",
    "@angular/flex-layout": "^2.0.0-beta.10-4905443",
    "@angular/forms": "6.0.0-rc.1",
    "@angular/http": "6.0.0-rc.1",
    "@angular/material": "^5.0.0-rc.2",
    "@angular/platform-browser": "6.0.0-rc.1",
    "@angular/platform-browser-dynamic": "6.0.0-rc.1",
    "@angular/router": "6.0.0-rc.1",
    "bootstrap": "^3.3.7",
    "core-js": "^2.4.1",
    "hammerjs": "^2.0.8",
    "rxjs": "6.0.0-rc.1",
    "rxjs-compat": "6.0.0-rc.1",
    "zone.js": "^0.8.26"

## This project uses most of the Angular Features (Upgraded to 6.0.0-rc.1) (As of April 2018)

a) Lazy loading of modules

b) Preloading Strategy Implementation - Loads only modules with route attribute set as "preload=true"

c) Segregation of various modules (Core, Shared and Products modules)

d) Products Module - Has implementation of cdk-table and MatDialogs for Add/Edit pages

e) User Profile - Has Reactive Forms implemenation with most validations

f) Dynamic Controls creation for adding more addresses to User Profile using Forms Array control collection

g) Ahead of Time Compilation

h) LESS based styling (UI Cleanup/Design work in progress)

## Project Structure
```

|app
|-----_core
|
|-----_guards
|
|-----_models
|
|-----_services
|
|-----_shared
|
|-----products
|
|-----assets
|
|-----environments
|
|-----less
```

This Application has three modules

a) Core Module

b) Shared Module

c) Products Module (This module is Lazily loaded using Selective Preloading Strategy)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
